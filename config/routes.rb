Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :titles

  root 'titles#home'

  get 'titles/home'
  get 'titles/index'
  get 'titles/about'
  get 'titles/help'

  get '/home',    to: 'titles#home'
  get '/index',   to: 'titles#index'
  get '/about',   to: 'titles#about'
  get '/help',    to: 'titles#help'

  post '/home',   to: 'titles#home'

end
