class CreateTitles < ActiveRecord::Migration[5.1]

  def change
    create_table :titles do |t|
      t.string :uri
      t.string :email
      t.string :title
      t.string :location
      t.timestamps
    end
  end

end
