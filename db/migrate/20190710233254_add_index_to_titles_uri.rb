class AddIndexToTitlesUri < ActiveRecord::Migration[5.1]

  def change
    add_index :titles, :uri, unique: true
  end

end
