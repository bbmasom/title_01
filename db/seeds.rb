# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Title.create!(uri:      "www.seznam.cz",
             email:     "me01@email.com",
             title:     "Seznam, najdu tam co neznám",
             location:  "Kosire 150 00, CZ, Stredocesky kraj")

Title.create!(uri:      "edition.cnn.com",
             email:     "me02@email.com",
             title:     "CNN International - Breaking News, US News, World News and Video",
             location:  "US")

Title.create!(uri:      "www.weather-forecast.com",
             email:     "me03@email.com",
             title:     "10 Day Weather Forecast Worldwide",
             location:  "St Louis 63101, US, Missouri")
