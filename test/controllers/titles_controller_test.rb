require 'test_helper'

class TitlesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    get root_path
    assert_response :success
  end

  test "should get index" do
    get index_path
    assert_response :success
  end

  test "should get about" do
    get about_path
    assert_response :success
  end

  test "should get help" do
    get help_path
    assert_response :success
  end

end
