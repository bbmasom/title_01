# Preview all emails at http://localhost:3000/rails/mailers/title_mailer
class TitleMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/title_mailer/send_result
  def send_result
    title = Title.first
    TitleMailer.send_result(title)
  end

end
