require 'test_helper'

class TitleMailerTest < ActionMailer::TestCase
  test "send_result" do
    title = titles(:one)
    mail = TitleMailer.send_result(title)
    assert_equal "Requested details - title and location", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
