require 'test_helper'

class TitleTest < ActiveSupport::TestCase

  # test "the truth" do
  #   assert true
  # end

  def setup
    @title = Title.new(uri: "https://www.ctk.cz/", email: "me001@email.com")
  end

  test "should be valid" do
    assert @title.valid?
  end

  test "uri should be present" do
    @title.uri = "     "
    assert_not @title.valid?
  end
  
  test "email should be present" do
    @title.email = "     "
    assert_not @title.valid?
  end
  
  test "uri should not be too long" do
    @title.uri = "a" * 151
    assert_not @title.valid?
  end

  test "email should not be too long" do
    @title.email = "a" * 141 + "@email.com"
    assert_not @title.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @title.email = valid_address
      assert @title.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @title.email = invalid_address
      assert_not @title.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "uri should be unique" do
    duplicate_title = @title.dup
    duplicate_title.uri = @title.uri.upcase
    @title.save
    assert_not duplicate_title.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @title.email = mixed_case_email
    @title.save
    assert_equal mixed_case_email.downcase, @title.reload.email
  end
  
end
