class TitleMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.title_mailer.send_result.subject
  #
  def send_result(title)
    @title = title
    mail to: title.email, subject: "Requested details - title and location"
  end
end
