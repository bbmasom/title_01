class Title < ApplicationRecord

  before_save   :downcase_email

  validates :uri, presence: true, length: { maximum: 150 },
                  uniqueness:
                    { case_sensitive: false, message: " has already been searched" }
                    #{ case_sensitive: false, message: "%{attribute} is duplicated" }
                    #{ case_sensitive: false, message: ->(object, data) do "Hey #{object.uri}!, #{data[:value]} is taken already!" end }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :email, presence:   true, length: { maximum: 150 },
                    format:     { with: VALID_EMAIL_REGEX }

  # Sends search result email.
  def send_result_email
    TitleMailer.send_result(self).deliver_now
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      email.downcase!
    end

end
