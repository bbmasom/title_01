require 'mechanize'
require 'socket'

class TitlesController < ApplicationController

  # --- HOME ---

  def home
    
#    if !@title.nil?
#      puts "Errors count_c: " + @title.errors.count.to_s
#    end
#    if @title.nil?
#      @title = Title.new
#    else
#      debugger      
#    end
#    puts "Errors count_d: " + @title.errors.count.to_s
    
    @title = Title.new
    @titles = Title.all.order("created_at DESC")

#    if @title.nil?
#      @title = Title.new
#    end

#    if @title_err.nil?
#      @title = Title.new
#    else
#      @title = @title_err
#    end

  end

  def index
    @titles = Title.all.order("created_at DESC").paginate(page: params[:page])
  end

  # --- ABOUT ---

  def about
  end

  # --- HELP ---

  def help
  end

  # --- CREATE ---

  def create

    @title = Title.new(title_params)
    @mechanize = Mechanize.new
    
    @uri_ok = false
    @uri_ok = @title.uri.downcase.include? "https://"
    if !@uri_ok
      @uri_ok = @title.uri.downcase.include? "http://"
    end

    #debugger

    if !@uri_ok
      
      begin
        @mechanize_page = @mechanize.get('https://' + @title.uri)
      rescue => e
        @mechanize_page = nil
        p e
      end      

      #debugger

      if @mechanize_page.nil?

        begin
          @mechanize_page = @mechanize.get('http://' + @title.uri)
        rescue => e
          @mechanize_page = nil
          p e
        end      

      end

    else
      
      begin
        @mechanize_page = @mechanize.get(@title.uri)
      rescue => e
        @mechanize_page = nil
        p e
      end      

    end
    
    #debugger

    if @mechanize_page.nil?
      @title.errors.add(:base, "Invalid URL")
    end

    #debugger

    if @title.errors.count == 0
      @ip_address = IPSocket::getaddress(@mechanize_page.uri.host)
      @result_geo = Geocoder.search(@ip_address)
      @title.uri = @mechanize_page.uri.host.to_s
#      @title.uri = @mechanize_page.uri.to_s
      @title.title = @mechanize_page.title
      @title.location = @result_geo.first.address + ", " + @result_geo.first.region
      if verify_recaptcha(model: @title) && @title.save
      #if @title.save
        @title.send_result_email
        flash[:info] = "Please check your email for requested details."
        redirect_to root_url
      else
# zde by mozna byla moznost najit chybu(chyby) URI a zmenit ji na URL
        #@title_err = @title
        #debugger
#        puts "Errors count (A): " + @title.errors.count.to_s
#        puts "Errors list (A): " + @title.errors.full_messages.to_s
        @titles = Title.all.order("created_at DESC")
        render 'home'
      end
    else
#        puts "Errors count (B): " + @title.errors.count.to_s
#        puts "Errors list (B): " + @title.errors.full_messages.to_s
        @titles = Title.all.order("created_at DESC")
        render 'home'
    end

  end

  # --- SHOW ---

  def show
    @title = Title.find(params[:id])
  end

  #def new
  #  @title = Title.new
  #  render 'home'
  #end

  def destroy
    Title.find(params[:id]).destroy
    flash[:success] = "Row deleted"
    #redirect_to index_url
    redirect_to root_url
  end

  private

    def title_params
#      params.require(:title).permit(:uri, :email, :title, :location)
      params.require(:title).permit(:uri, :email)
    end

end
