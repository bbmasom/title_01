class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  def hello
    render html: "Hi, welcome on Title & location!"
  end

end
